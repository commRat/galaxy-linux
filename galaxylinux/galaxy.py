import sys
import subprocess
import requests
import json
import logging
import threading
import time
import os
from version import __version__
import web


def run_command(command):
    return subprocess.run(command, shell=True, capture_output=True)


def get_disks():
    command = "lsblk -d -J"
    output = run_command(command)
    return json.loads(output.stdout.decode())


def get_disks_names(disks=get_disks()):
    return [disk.get("name") for disk in disks.get("blockdevices")]


def get_distributions():
    return ["arch"]


def install(distribution, details):
    return subprocess.call([
        'sh',
        f'galaxylinux/scripts/{distribution}.sh',
        f'{details.get("disk")}',
        f'{details.get("wifi_name")}',
        f'{details.get("wifi_pwd")}',
        f'{details.get("user")}',
        f'{details.get("password")}',
    ])


def connection_create(connection_details):
    command = f"nmcli dev wifi connect {connection_details.get('wifi_name')} password {connection_details.get('wifi_pwd')}"
    return {"connection_create": run_command(command).returncode}


def connection_confirm():
    try:
        requests.get("http://77.75.75.172", timeout=5)
        return {"connection_confirm": 0}
    except (requests.ConnectionError, requests.Timeout):
        return {"connection_confirm": "Network is unreachable"}


def main():
    logging.basicConfig(level='INFO')
    logging.info("Starting Galaxy Linux v%s" % __version__)
    thr_web = threading.Thread(target=web.start)
    thr_web.start()
    run_command("xdg-open http://127.0.0.1:5000")
    disks_names = get_disks_names()
    while 1:
        time.sleep(1)
    return 0


if __name__ == '__main__':
    sys.exit(main())
