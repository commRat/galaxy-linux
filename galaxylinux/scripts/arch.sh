#!/bin/bash
set -e -x

DEV=/dev/$1
WIFI_NAME=$2
WIFI_PWD=$3
USER=$4
PASSWORD=$5
MNT=/mnt

# TODO: read wifi connection details from web
echo Enter wifi-name You want to connect:
read varname
read -sp 'Enter password: ' passvar
echo Success

dd if=/dev/zero of=${DEV} bs=1M count=1
partprobe ${DEV}

# TODO: maybe user choice?
parted ${DEV} --script mklabel msdos
parted -a optimal ${DEV} --script mkpart primary fat32 160 2000 set 1 boot on
parted -a optimal ${DEV} --script mkpart primary linux-swap 2000 9000
parted -a optimal ${DEV} --script mkpart primary ext4 9000 100%

mkfs.ext4 ${DEV}3 -FF
mkswap ${DEV}2
mkfs.vfat ${DEV}1

mount ${DEV}3 ${MNT}
mkdir ${MNT}/boot
mount ${DEV}1 ${MNT}/boot
swapon ${DEV}2

iwctl -P ${passvar} station wlan0 connect ${varname}
sleep 2
echo 'Server = http://mirror.dkm.cz/archlinux/$repo/os/$arch' >/etc/pacman.d/mirrorlist
sleep 2
/usr/lib/systemd/systemd-networkd-wait-online -i wlan0

pacstrap ${MNT} base linux linux-firmware
genfstab -U ${MNT} >> ${MNT}/etc/fstab
cp /var/lib/iwd/${varname}.psk /mnt/var/lib/iwd

(echo pacman -Syu iwd --noconfirm) | arch-chroot ${MNT}
(echo pacman -Syu grub --noconfirm) | arch-chroot ${MNT}
(echo grub-install ${DEV}) | arch-chroot ${MNT}
(echo grub-mkconfig -o /boot/grub/grub.cfg) | arch-chroot ${MNT}
(echo pacman -Syu dhcpcd --noconfirm) | arch-chroot ${MNT}
(echo useradd -m archuser) | arch-chroot ${MNT}

(
echo 'root:changeme'
echo 'archuser:changeme'
) | arch-chroot ${MNT} chpasswd

(echo pacman -Syu sddm networkmanager --noconfirm) | arch-chroot ${MNT}
(echo systemctl enable iwd) | arch-chroot ${MNT}
(echo systemctl enable sddm.service) | arch-chroot ${MNT}
(echo systemctl enable NetworkManager.service) | arch-chroot ${MNT}
(echo pacman -Syu --noconfirm) | arch-chroot ${MNT}
(echo pacman -Syu xorg plasma plasma-wayland-session kde-applications --noconfirm) | arch-chroot ${MNT}

umount -R ${MNT}
