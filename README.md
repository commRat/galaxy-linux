# Galaxy Linux

The goal of this project is to create bootable, small size ISO image. After booting to new system, user picks distribution of his choice and install It.

## Distributions

### In Progress

- Arch Linux

### Scheduled

- Fedora
- Slackware
- Debian
- Ubuntu
